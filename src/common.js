'use strict'

const gridConfig = {
  rowHeight: '30px',
  refresh: true,
  labels: {
    columns: 'Стовпці',
    allCols: 'Усі стовпці',
    rows: 'Рядки',
    selected: {
      singular: 'елемент обраний.',
      plural: 'елементів обрано.'
    },
    clear: 'Очистити',
    search: 'Пошук',
    all: 'Усі'
  },
  messages: {
    noData: '<i class="q-icon material-icons">warning</i><span>Немає доступних для перегляду даних.</span>',
    noDataAfterFiltering: '<i class="q-icon material-icons">warning</i><span>Нічого не знайдено. Будь ласка, уточніть умови пошуку.</span>'
  },
  selection: 'single'
}

const dedur = (dur) => {
  let lDur = Math.floor(dur / 1000)
  const vm = 60
  const vh = vm * 60
  const vd = vh * 24
  const fd = Math.floor(lDur / vd)
  const h = lDur % vd
  const fh = Math.floor(h / vh)
  const m = h % vh
  const fm = Math.floor(m / vm)
  const fs = m % vm
  return `${fd ? fd + 'd ' : ''}${fh ? fh + 'h ' : ''}${fm ? fm + 'm ' : ''}${fs ? fs + 's' : ''}`.trim()
}

export {
  gridConfig,
  dedur
}
