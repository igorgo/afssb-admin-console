'use strict'

import { SessionStorage } from 'quasar-framework'

let adminToken

function getAdminToken () {
  adminToken = SessionStorage.get.item('token')
  return adminToken
}

function setAdminToken (token) {
  SessionStorage.set('token', token)
}

function isTokenSet () {
  return !!adminToken
}

function clearToken () {
  SessionStorage.remove('token')
}

getAdminToken()

export {
  adminToken,
  isTokenSet,
  setAdminToken,
  clearToken,
  getAdminToken
}
