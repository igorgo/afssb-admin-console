'use strict'
let paths = window.location.pathname.split('/')
const i = paths.indexOf('arm-admin')
const urlPrefix = window.location.origin + paths.slice(0, i).join('/') + `/admin`
// const urlPrefix = `http://localhost:8416/admin`
import {getAdminToken} from './auth'
import request from 'node-fetch'
import {Toast} from 'quasar-framework'

async function rest ({ method, endPoint, params = [], data = {} }) {
  let url = urlPrefix + endPoint
  let status = 500, payload = {}
  const options = {
    method,
    headers:
      {
        'cache-control': 'no-cache',
        'X-Uasb-Token': getAdminToken(),
        'content-type': 'application/json'
      }
  }
  if (method === 'GET' || method === 'DELETE') {
    params.forEach(p => {
      url += '/' + p
    })
  }
  else {
    options.body = JSON.stringify(data)
  }
  try {
    const t = await request(url, options)
    status = t.status
    if (status !== 204) payload = await t.json()
    if (status >= 400) {
      Toast.create['negative']({
        html: `${status}(${payload.code}). ${payload.message}`
      })
    }
  }
  catch (e) {
    console.error(e)
  }
  return {
    status,
    payload
  }
}

export {
  rest
}
